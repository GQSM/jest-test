const async = require('../funcs/async.js')

test('test async', (done) => {
    const callBack = (data) => {
        expect(data).toBe('getData')
        done()
    }
    async.fetchData(callBack)
})

test('test promise async', () => {
    return async.promiseFetchData()
        .then((data) => {
            expect(data).toBe('getData')
        })
})

test('test rejects in promise', () => {
    return expect(async.promiseErrorFetchData()).rejects.toBe('error')
})

test('test use await in async', async () => {
    expect.assertions(1)
    const fetchData = (name) => {
        setTimeout(() => {
            return `Hello ${name}!`
        }, 3000)
    }
    const data = await fetchData('GQSM')
    expect(data).toBe('Hello GQSM!')
})
