//模擬資料庫的資料
const userDB = [
    { id: 1, name: '小明' },
    { id: 2, name: '小華' },
]

//新增測試資料
/* callBack 方式
const insertTestData = (data, callBack) => {
    setTimeout(() => {
        userDB.push(data)
        callBack()
    }, 3000)
}
*/

//回傳 Promise 的 Function
const insertTestData = (data) => {
    //回傳 Prmoinse 物件
    return new Promise((resolve,reject) => {
        setTimeout(() => {
            userDB.push(data)

            //在 Timeout 中執行後呼叫 resolve()
            resolve()
        }, 3000)
    })
}

//移除測試資料
const deleteTestData = id => {
    let findIndex = userDB.findIndex((user) => {
        return user.id === id
    })
    if (findIndex !== -1)
        userDB.splice(findIndex, 1)
}

//查詢測試資料
const getUserData = id => {
    let goalData = userDB.find((user) => {
        return user.id === id
    })
    return goalData
}

//全部測試完後確認資料狀態
afterAll(() => {
    console.log(userDB)
})

describe('Test about user data', () => {

    /* callBack 方式
    //傳入 done 參數
    beforeAll(done => {
        //callBack 函式，會在 Timeout 後執行
        const callBack = () => {
            //當執行完 Timeout ，進入 callBack 後就能結束
            done()
        }

        //將 callBack 函式傳入 insertTestData
        insertTestData({ id: 99, name: '測試人員' }, callBack)
    })
    */


    beforeAll(() => {
        //藉由 return 接收到的 Promise 處理異步請求
        return insertTestData({ id: 99, name: '測試人員' })
    })

    //結束時清除測試資料
    afterAll(() => {
        deleteTestData(99)
    })

    //確認是否回傳正確的資料
    test('Test get user data', () => {
        expect(getUserData(99)).toEqual({ id: 99, name: '測試人員' })
    })

})