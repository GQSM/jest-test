const sum = (x = 0, y = 0) => {
    return (isNaN(x) ? 0 : x) + (isNaN(y) ? 0 : y)
}

const square = (x = 0) => {
    let intX = isNaN(x) ? 0 : x
    return intX * intX
}

describe('Test sum', () => {
    test('Test default return zero', () => {
        expect(sum()).toBe(0)
    })

    test('Test 3 plus 5 is 8', () => {
        expect(sum(3, 5)).toBe(8)
    })

    test('Pass when value is NaN can return zero', () => {
        expect(sum(NaN, NaN)).not.toBeNaN()
    })
})

describe('Test square', () => {
    const obj = 'a'
    test('Pass 3 can return 9', () => {
        expect(square(3)).toBe(9)
    })

    test('Pass when value is String can return zero', () => {
        expect(square('efg')).toBe(0)
    })
})
