const sum = (a, b) => {
    return a + b
}

test('Check the result of 5 + 2', () => {
    expect(sum(5, 2)).toBe(7)
})

test('Check the object type', () => {
    const peopleA = {
        name: 'GQSM',
    }
    peopleA.age = 25

    // 測試字串
    expect(peopleA.name).toBe('GQSM')
    // 測試物件
    expect(peopleA).toEqual({ name: 'GQSM', age: 25 })
})

test('Use not', () => {
    const peopleA = {
        name: 'GQSM',
    }
    // check name not empty
    expect(peopleA.name).not.toBe('')
})

test('test integer', () => {
    // 整數可以使用 toBe 或 toEqual 斷言
    expect(5).toBe(5)
    expect(5).toEqual(5)

    // 測試輸出值是否大於期望值
    expect(5).toBeGreaterThan(4)

    // 測試輸出值是否大於等於期望值
    expect(5).toBeGreaterThanOrEqual(5)

    // 測試輸出值是否小於期望值
    expect(5).toBeLessThan(6)

    // 測試輸出值是否小於期望值
    expect(5).toBeLessThanOrEqual(5)
})

test('Test float', () => {
    // 會忽略些微的誤差
    expect(0.1 + 0.2).toBeCloseTo(0.3)
})

// Match
test('Use toMatch in test', () => {
    // 搭配正規表達式
    expect('Happy new year.').not.toMatch(/New/)
})

// Array
test('For array test in jest', () => {
    const arrA = ['A', 'B', 'C']

    // 檢查陣列內是否含有某值
    expect(arrA).toContain('B')

    // 搭配迴圈檢查每個位置都不等於空
    for (let i; arrA.length - 1; i += 1) {
        expect(arrA[i]).not.toBe('')
    }
})

// special value
test('Special value', () => {
    // 期望值為 true
    expect(true).toBeTruthy()

    // 期望值為 false
    expect(false).toBeFalsy()

    // 期望值為 null
    expect(null).toBeNull()

    // 期望值為 undefined
    expect(undefined).toBeUndefined()

    // 期望值為 undefined 之外的值
    expect(null).toBeDefined()
})

const sayHello = (name) => {
    if (name) {
        return `Hello ${name}!`
    }
    return 'Hello!'
}

test('Test Coverage', () => {
    expect(sayHello()).toBe('Hello!')
})
