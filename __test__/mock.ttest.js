


// import axios 模組
const axios = require('axios')

// 取得所有產品
const getAllGoods = () => {
    // 使用 axios 中的 get Function 獲得資料
    return axios.get('http://httpbin.org/get').then((resp) => {
        console.log(resp)
        return resp.data
    })
}

jest.mock('axios')

test('should fetch users', () => {
    const goods = [{ name: 'Milk' }, { name: 'Apple' }]
    const res = { data: goods }
    // 為 axios 中的 get 模擬回傳值為 res
    axios.get.mockResolvedValue(res)

    return getAllGoods().then((resp) => {
        console.log(resp)
        expect(resp[0].name).toBe('Milk')
    })
})
