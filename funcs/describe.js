const sum = (x = 0, y = 0) => {
    return (Number.isNaN(x) ? 0 : x) + (Number.isNaN(y) ? 0 : y)
}


module.exports = {
    sum,
}
