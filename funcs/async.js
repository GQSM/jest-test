const fetchData = (callBack) => {
    setTimeout(() => {
        callBack('getData')
    }, 3000)
}

const promiseFetchData = () => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve('getData')
        }, 3000)
    })
}

const promiseErrorFetchData = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject(new Error('error'))
        }, 3000)
    })
}

module.exports = {
    fetchData,
    promiseFetchData,
    promiseErrorFetchData,
}
