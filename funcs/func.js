const sum = (a, b) => {
    if (b) {
        return a + b
    }
    return a
}

module.exports = {
    sum,
}
